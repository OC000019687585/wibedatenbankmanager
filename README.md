Der WiBe Datenbank Manager ist eine Standalone Java Anwendung mit geringen Abhänigkeiten. 

Sie soll das schnelle erstellen von Usern, Datenbanken und wibe.properties Dateien für den [WiBe Kalkulator](https://www.cio.bund.de/Webs/CIO/DE/digitaler-wandel/Achitekturen_und_Standards/IT_Architektur_Bund/wirtschaftlichkeitsbetrachtung/wibe-kalkulator/wibe-kalkulator-node.html) ermöglichen ohne in der CLI oder Drittprogrammen des Datenbankservers tätig sein zu müssen.

Für die Nutzung muss die beispiel.env in .env umbenannt und mit den Datenbankserverdaten (hostname/Ip Adressse, Benutzername eines Users der neue Datenbanken anlegen darf und dessen Passwort) gefüllt werden. Danach kann per mvn package eine Jar Datei erzeugt werden, die die komplette Anwendung enhält.

Zur Zeit sind nur MySQL Datenbanken getestet und unterstützt, die Unterstütztung für die anderen vom WiBe Kalkualtor unterstützten Datenbanken kann gerne nachgefplegt werden.
