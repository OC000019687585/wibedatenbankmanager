package de.bremen;

import java.util.ArrayList;
import java.util.stream.Stream;

import io.github.cdimascio.dotenv.Dotenv;

public class WiBePropertiesGenerator {
    String username;
    Boolean fürAdmin;
    ArrayList<String> berechtigteDBs;
    String wibeProperties;


    public WiBePropertiesGenerator(String username) {
        this.username = username;
        this.fürAdmin = fürAdmin;

        this.berechtigteDBs = BerechtigungsManager.getBerechtigteDatenbankenfürUser(username);
        generatewibeProperties();
    }

    private void generatewibeProperties(){
        this.wibeProperties = "";

        // Berechtigte Datenbanken einfügen
        this.wibeProperties = this.wibeProperties.concat("Datenbanken = ");
        for(String dbname : this.berechtigteDBs){
            this.wibeProperties = this.wibeProperties.concat(dbname+ ",");
        }
        this.wibeProperties = this.wibeProperties.concat(System.lineSeparator() + System.lineSeparator() );

        // Ermittle Treiber Protokoll Dialekt Connection String
        String treiber = getTreiber();
        String protokoll = getProtokoll();
        String dialekt = getDialekt();
        String datenbankConnecString = getDatenbankConnectString();

        // Gib die Parameter für jede Datenbank aus
        for(String dbname : this.berechtigteDBs){
            this.wibeProperties = this.wibeProperties.concat(dbname + ".Datenbank=" + datenbankConnecString + dbname + System.lineSeparator());
            this.wibeProperties = this.wibeProperties.concat(dbname + ".Treiber=" + treiber + System.lineSeparator());
            this.wibeProperties = this.wibeProperties.concat(dbname + ".Protokoll=" + protokoll + System.lineSeparator());
            this.wibeProperties = this.wibeProperties.concat(dbname + ".Dialekt=" + dialekt + System.lineSeparator());
            this.wibeProperties = this.wibeProperties.concat(System.lineSeparator());
        }

        // Gib die Benutzernamen und Passwort aus. Wenns der DBAdmin nim die Daten aus der ENV
        Dotenv dotenv = Dotenv.load();
        if(this.username.equals(dotenv.get("dbusername"))){            
            this.wibeProperties = this.wibeProperties.concat("Benutzer=" + dotenv.get("dbusername") + System.lineSeparator());
            this.wibeProperties = this.wibeProperties.concat("Kennwort=" + dotenv.get("dbpasswort") + System.lineSeparator());
            dotenv = null;
        }else{
            this.wibeProperties = this.wibeProperties.concat("Benutzer=" + this.username + System.lineSeparator());
            this.wibeProperties = this.wibeProperties.concat("Kennwort=" + Anwender.getPWforUser(username) + System.lineSeparator());
        }
        this.wibeProperties = this.wibeProperties.concat(System.lineSeparator());

        // restliches BlaBla
        this.wibeProperties = this.wibeProperties.concat("Breite=960" + System.lineSeparator());
        this.wibeProperties = this.wibeProperties.concat("Hoehe=640" + System.lineSeparator());
        this.wibeProperties = this.wibeProperties.concat("AnzahlAnmeldungen=5" + System.lineSeparator());
        this.wibeProperties = this.wibeProperties.concat("Sicherheitssystem=ein" + System.lineSeparator());
        this.wibeProperties = this.wibeProperties.concat("Berechnungen=de.bund.kbst.wibe.Berechnungen" + System.lineSeparator());
        this.wibeProperties = this.wibeProperties.concat("Notizverwaltung=de.bund.kbst.wibe.Notizverwaltung" + System.lineSeparator());
        this.wibeProperties = this.wibeProperties.concat("Reporting=de.bund.kbst.wibe.Reporting" + System.lineSeparator());
    }

    private String getDatenbankConnectString() {
        Dotenv dotenv = Dotenv.load();
        String dbconnectstring = dotenv.get("dbconnectstring");
        if (dbconnectstring.endsWith("/")) {

        } else {
            dbconnectstring = dbconnectstring + "/";
        }
        return dbconnectstring;
    }

    private String getDialekt() {
        Dotenv dotenv = Dotenv.load();
        // ToDo: andere Datenbanken einfügen
        switch (dotenv.get("dbdialekt")) {
            case "mariadb":
                return "org.hibernate.dialect.MySQL8Dialect";
        }
        // ToDo: wirf einen ordentlichen Fehler bitte
        return "";
    }

    private String getProtokoll() {
        Dotenv dotenv = Dotenv.load();
        // ToDo: andere Datenbanken einfügen
        switch (dotenv.get("dbdialekt")) {
            case "mariadb":
                return "jdbc:mysql:";
        }
        // ToDo: wirf einen ordentlichen Fehler bitte
        return "";
    }

    private String getTreiber() {
        Dotenv dotenv = Dotenv.load();
        // ToDo: andere Datenbanken einfügen
        switch (dotenv.get("dbdialekt")) {
            case "mariadb":
                return "com.mysql.jdbc.Driver";
        }
        // ToDo: wirf einen ordentlichen Fehler bitte
        return "";
    }

    
    public String getWibeProperties() {
        return wibeProperties;
    }

}
