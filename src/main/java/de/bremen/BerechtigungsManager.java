package de.bremen;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BerechtigungsManager {
    public static ArrayList<String> getBerechtigteDatenbankenfürUser(String username) {
        ArrayList<String> berechtigteDatenbanken = new ArrayList<String>();
        try {
            ArrayList<String> parameters = new ArrayList<String>();
            parameters.add(username);
            ResultSet rs = SQLManager.SQLSelect("SHOW GRANTS FOR ?", parameters);
            while (rs.next()) {
                String pattern = "`(.+?)`\\.\\*";
                Pattern r = Pattern.compile(pattern);
                Matcher m = r.matcher(rs.getString(1));
                if (m.find()) {
                    berechtigteDatenbanken.add(m.group(1));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return berechtigteDatenbanken;
    }

    public static ArrayList<String> getBerechtigteUserfürDatenbank(String datenbankname) {
        ArrayList<String> berechtigteUser = new ArrayList<String>();
        ArrayList<String> alleUser = Anwender.getallAnwender();
        ArrayList<String> parameters = new ArrayList<String>();
        for (String user : alleUser) {
            try {
                parameters = new ArrayList<String>();
                parameters.add(user);
                ResultSet rs = SQLManager.SQLSelect("SHOW GRANTS FOR ?", parameters);
                while (rs.next()) {
                    String pattern = "`(.+?)`\\.\\*";
                    Pattern r = Pattern.compile(pattern);
                    Matcher m = r.matcher(rs.getString(1));
                    if (m.find()) {
                        if (m.group(1).toLowerCase().equals(datenbankname.toLowerCase())) {

                            berechtigteUser.add(user);
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return berechtigteUser;
    }
}
