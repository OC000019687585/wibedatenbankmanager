package de.bremen;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.ArrayList;

import io.github.cdimascio.dotenv.Dotenv;

public class SQLManager {
    private static String getJDBCConnectionString() {
        // ToDo andere Datenbanken einfügen
        Dotenv dotenv = Dotenv.load();
        String jdbcString = "";
        switch (dotenv.get("dbdialekt")) {
            case "mariadb":
                jdbcString = "jdbc:mariadb:" + dotenv.get("dbconnectstring");
                break;
            default:
                break;
        }
        return jdbcString;
    }

    public static void SQLExceuteUnsafe(String sql) {

        Dotenv dotenv = Dotenv.load();
        try (Connection con = DriverManager.getConnection(getJDBCConnectionString(), dotenv.get("dbusername"),
                dotenv.get("dbpasswort"));) {

            Statement statement = con.createStatement();
            statement.execute(sql);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void SQLExecute(String sql, ArrayList parameters) {

        Dotenv dotenv = Dotenv.load();
        try (Connection con = DriverManager.getConnection(getJDBCConnectionString(), dotenv.get("dbusername"),
                dotenv.get("dbpasswort"));
                PreparedStatement ps = con.prepareStatement(sql)) {
            Integer forCounter = 1;
            for (Object parameter : parameters) {
                switch (parameter.getClass().getName()) {
                    case "java.lang.Integer":
                        ps.setInt(forCounter, (Integer) parameter);
                        break;
                    case "java.lang.String":
                        ps.setString(forCounter, (String) parameter);
                        break;
                    default:
                        break;
                }
                forCounter++;
            }
            try {

                ps.executeQuery();
            } catch (Exception e) {
                e.printStackTrace();
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static ResultSet SQLSelect(String sql, ArrayList parameters) throws SQLException {
        ResultSet resultSet;
        Dotenv dotenv = Dotenv.load();
        Connection con = DriverManager.getConnection(getJDBCConnectionString(), dotenv.get("dbusername"),
                dotenv.get("dbpasswort"));
        PreparedStatement ps = con.prepareStatement(sql);
        Integer forCounter = 1;
        for (Object parameter : parameters) {
            switch (parameter.getClass().getName()) {
                case "java.lang.Integer":
                    ps.setInt(forCounter, (Integer) parameter);
                    break;
                case "java.lang.String":
                    ps.setString(forCounter, (String) parameter);
                    break;
                case "java.lang.Date":
                    ps.setDate(forCounter, (Date) parameter);
                    break;
                default:
                    break;
            }
            forCounter++;
        }
        resultSet = ps.executeQuery();
        con.close();
        return resultSet;
    }
}
