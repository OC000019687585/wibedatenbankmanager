package de.bremen;

import java.awt.List;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import io.github.cdimascio.dotenv.Dotenv;

public class Datenbanken {
    public static ArrayList<String> getalleDatenbanken() {
        ArrayList<String> datenbankens = new ArrayList<String>();

        // Ermitle den SQL Query für die genutze Datenbank
        Dotenv dotenv = Dotenv.load();
        String sqlString = "";

        // ToDo: andere Datenbanken einfügen
        switch (dotenv.get("dbdialekt")) {
            case "mariadb":
                sqlString = "show databases;";
                break;
            default:
                break;
        }
        try {
            ResultSet rs = SQLManager.SQLSelect(sqlString, new ArrayList());
            while (rs.next()) {
                datenbankens.add(rs.getString(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return filterSystemDBs(datenbankens);
    }

    private static ArrayList<String> filterSystemDBs(ArrayList<String> datenbankens){
        
        Dotenv dotenv = Dotenv.load();
        ArrayList<String> systemDB = new ArrayList<String>();
        systemDB.add("wibedbmanager");
        // ToDo: andere Datenbanken einfügen
        switch (dotenv.get("dbdialekt")) {
            case "mariadb":
                systemDB.add("information_schema");
                systemDB.add("mysql");
                systemDB.add("performance_schema");
                systemDB.add("sys");
                
                datenbankens.removeAll(systemDB);
                break;
            default:
                break;
        }
        return datenbankens;
    }

    public static Boolean checkifDBExists(String dbName){
        ArrayList<String> existingDBNamen = new ArrayList<String>(getalleDatenbanken());
        if(existingDBNamen.contains(dbName) == true){
            return true;
        }
        return false;
    }
}
