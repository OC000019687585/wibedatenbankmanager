package de.bremen.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import de.bremen.Anwender;
import de.bremen.BerechtigungsManager;
import de.bremen.Datenbanken;
import de.bremen.SQLManager;
import io.github.cdimascio.dotenv.Dotenv;

public class BerechtigungenPanel extends JPanel {
    public static JPanel getBerechtigungenPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(1, 3, 10, 0));

        JPanel userDBPanel = new JPanel(new BorderLayout());
        JRadioButton radUser = new JRadioButton("User", true);
        radUser.setName("radUser");
        JRadioButton radDB = new JRadioButton("Datenbanken");
        radDB.setName("radDB");
        ButtonGroup buttonGroup = new ButtonGroup();
        JPanel btnGroupPanel = new JPanel();

        JList userDBList = new JList<>();
        JScrollPane userDBListscrollPane = new JScrollPane();
        userDBListscrollPane.setViewportView(userDBList);
        userDBList.setLayoutOrientation(JList.VERTICAL);
        userDBList.setName("userDBList");
        userDBList.setListData(Anwender.getallAnwender().toArray());

        buttonGroup.add(radUser);
        buttonGroup.add(radDB);
        btnGroupPanel.add(radUser);
        btnGroupPanel.add(radDB);
        userDBPanel.add(btnGroupPanel, BorderLayout.PAGE_START);
        userDBPanel.add(userDBList, BorderLayout.CENTER);

        JPanel showPanel = new JPanel(new BorderLayout());
        JList showDetailsList = new JList<>();
        JScrollPane showDetailsListscrollPane = new JScrollPane();
        showDetailsListscrollPane.setViewportView(showDetailsList);
        showDetailsList.setLayoutOrientation(JList.VERTICAL);
        showDetailsList.setName("showDetailsList");
        JPanel showDetailsPanel = new JPanel();
        JButton btndetailDelete = new JButton("Berechtigung entfernen >>");
        showDetailsPanel.add(btndetailDelete);
        showPanel.add(showDetailsPanel, BorderLayout.PAGE_START);
        showPanel.add(showDetailsList, BorderLayout.CENTER);

        JPanel addPanel = new JPanel(new BorderLayout());
        JList addList = new JList<>();
        JScrollPane addListscrollPane = new JScrollPane();
        addListscrollPane.setViewportView(addList);
        addList.setLayoutOrientation(JList.VERTICAL);
        addList.setName("addList");
        JPanel addbtnPanel = new JPanel();
        JButton btnadd = new JButton("<< Berechtigung hinzufügen");
        addbtnPanel.add(btnadd);
        addPanel.add(addbtnPanel, BorderLayout.PAGE_START);
        addPanel.add(addList, BorderLayout.CENTER);

        panel.add(userDBPanel);
        panel.add(showPanel);
        panel.add(addPanel);

        radUser.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    Component component = (Component) e.getSource();
                    JList userDBList = (JList) findComponentbyName(component, "userDBList");
                    JList showDetailsList = (JList) findComponentbyName(component, "showDetailsList");
                    ArrayList<String> empArrayList = new ArrayList<String>();
                    userDBList.setListData(Anwender.getallAnwender().toArray());
                    showDetailsList.setListData(empArrayList.toArray());
                }
            }

        });

        radDB.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    Component component = (Component) e.getSource();
                    JList userDBList = (JList) findComponentbyName(component, "userDBList");
                    JList showDetailsList = (JList) findComponentbyName(component, "showDetailsList");
                    ArrayList<String> empArrayList = new ArrayList<String>();
                    userDBList.setListData(Datenbanken.getalleDatenbanken().toArray());
                    showDetailsList.setListData(empArrayList.toArray());
                }
            }
        });

        userDBList.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                Component component = (Component) e.getSource();
                updateDetailsundAdd(component);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

        });

        btnadd.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Component component = (Component) e.getSource();

                JRadioButton radUser = (JRadioButton) findComponentbyName(component, "radUser");
                JRadioButton radDB = (JRadioButton) findComponentbyName(component, "radDB");
                JList showDetailsList = (JList) findComponentbyName(component, "showDetailsList");
                JList userDBList = (JList) findComponentbyName(component, "userDBList");
                JList addList = (JList) findComponentbyName(component, "addList");
                String selectedItem = (String) addList.getSelectedValue();
                String username = "";
                String datenbank = "";

                if (radUser.isSelected() == true) {
                    username = (String) userDBList.getSelectedValue();
                    datenbank = selectedItem;
                }

                if (radDB.isSelected() == true) {
                    datenbank = (String) userDBList.getSelectedValue();
                    username = selectedItem;
                }

                ArrayList parameters = new ArrayList();
                String sqlString = "";
                Dotenv dotenv = Dotenv.load();
                parameters.add(datenbank);
                parameters.add(username);
                // ToDo: andere Datenbanken einfügen
                switch (dotenv.get("dbdialekt")) {
                    case "mariadb":
                        sqlString = "GRANT select, insert, update, delete on TESTDB.* to ?@'%'";
                        sqlString = sqlString.replace("TESTDB", datenbank);
                        parameters = new ArrayList();
                        parameters.add(username);
                        break;
                    default:
                        break;
                }
                SQLManager.SQLExecute(sqlString, parameters);
                updateDetailsundAdd(component);
            }

        });

        btndetailDelete.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Component component = (Component) e.getSource();

                JRadioButton radUser = (JRadioButton) findComponentbyName(component, "radUser");
                JRadioButton radDB = (JRadioButton) findComponentbyName(component, "radDB");
                JList showDetailsList = (JList) findComponentbyName(component, "showDetailsList");
                JList userDBList = (JList) findComponentbyName(component, "userDBList");
                JList addList = (JList) findComponentbyName(component, "addList");
                String selectedItem = (String) showDetailsList.getSelectedValue();
                String username = "";
                String datenbank = "";

                if (radUser.isSelected() == true) {
                    username = (String) userDBList.getSelectedValue();
                    datenbank = selectedItem;
                }

                if (radDB.isSelected() == true) {
                    datenbank = (String) userDBList.getSelectedValue();
                    username = selectedItem;
                }

                ArrayList parameters = new ArrayList();
                String sqlString = "";
                Dotenv dotenv = Dotenv.load();
                parameters.add(datenbank);
                parameters.add(username);
                // ToDo: andere Datenbanken einfügen
                switch (dotenv.get("dbdialekt")) {
                    case "mariadb":
                        sqlString = "REVOKE ALL on TESTDB.* FROM ?@'%'";
                        sqlString = sqlString.replace("TESTDB", datenbank);
                        parameters = new ArrayList();
                        parameters.add(username);
                        break;
                    default:
                        break;
                }
                SQLManager.SQLExecute(sqlString, parameters);
                updateDetailsundAdd(component);
            }

        });
        return panel;
    }

    private static Component findComponentbyName(Component component, String componentName) {

        JFrame frame = (JFrame) SwingUtilities.getRoot(component);

        for (Component item : getAllComponents(frame)) {
            if (item.getName() == componentName) {
                return item;
            }
        }

        return new Component() {

        };
    }

    public static List<Component> getAllComponents(final Container c) {
        Component[] comps = c.getComponents();
        List<Component> compList = new ArrayList<Component>();
        for (Component comp : comps) {
            compList.add(comp);
            if (comp instanceof Container)
                compList.addAll(getAllComponents((Container) comp));
        }
        return compList;
    }

    private static void updateDetailsundAdd(Component component) {

        JRadioButton radUser = (JRadioButton) findComponentbyName(component, "radUser");
        JRadioButton radDB = (JRadioButton) findComponentbyName(component, "radDB");
        JList showDetailsList = (JList) findComponentbyName(component, "showDetailsList");
        JList userDBList = (JList) findComponentbyName(component, "userDBList");
        JList addList = (JList) findComponentbyName(component, "addList");

        String selectedItem = (String) userDBList.getSelectedValue();

        if (userDBList == null) {
            return;
        }

        if (radUser.isSelected() == true) {
            ArrayList<String> restDBs = new ArrayList<String>(Datenbanken.getalleDatenbanken());
            ArrayList<String> berechtigteDBs = BerechtigungsManager
                    .getBerechtigteDatenbankenfürUser(selectedItem);
            restDBs.removeAll(berechtigteDBs);
            showDetailsList
                    .setListData(berechtigteDBs.toArray());
            addList.setListData(restDBs.toArray());
        }

        if (radDB.isSelected() == true) {
            ArrayList<String> restUsers = new ArrayList<String>(Anwender.getallAnwender());
            ArrayList<String> berechtigteUser = BerechtigungsManager
                    .getBerechtigteUserfürDatenbank(selectedItem);
            restUsers.removeAll(berechtigteUser);
            showDetailsList
                    .setListData(berechtigteUser.toArray());
            addList.setListData(restUsers.toArray());
        }
    }
}
