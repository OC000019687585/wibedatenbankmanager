package de.bremen.swing;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import de.bremen.Anwender;
import de.bremen.Datenbanken;
import de.bremen.SQLManager;
import io.github.cdimascio.dotenv.Dotenv;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class DatenbankPanel extends JPanel {

    public static JPanel getDatenbankenPanel() {
        JPanel DatenbankenPanel = new JPanel();
        DatenbankenPanel.setLayout(new GridLayout(1, 2));

        JList DatenbankenListe = new JList<>(Datenbanken.getalleDatenbanken().toArray());
        JScrollPane DatenbankenListescrollPane = new JScrollPane();
        DatenbankenListescrollPane.setViewportView(DatenbankenListe);
        DatenbankenListe.setLayoutOrientation(JList.VERTICAL);
        JPanel buttonPanel = new JPanel();

        JButton DatenbankenAdd = new JButton("Datenbank hinzufügen");
        JButton DatenbankenDelete = new JButton("Datenbank löschen");
        DatenbankenDelete.setToolTipText("Löscht die ausgewählte Datenbanks");

        DatenbankenPanel.add(DatenbankenListe);
        buttonPanel.add(DatenbankenAdd);
        buttonPanel.add(DatenbankenDelete);
        DatenbankenPanel.add(buttonPanel);

        DatenbankenAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JButton btnAdd = (JButton) e.getSource();
                JPanel userPanel = (JPanel) btnAdd.getParent().getParent();
                JList userList = (JList) userPanel.getComponent(0);

                // ToDo: Prüfe den Datenbanknamen auf SQL Injections und validität
                String datenbankName = JOptionPane.showInputDialog(
                        (JFrame) SwingUtilities.getWindowAncestor((JButton) e.getSource()),
                        "Datenbankname der hinzugefügt werden soll.",
                        "Datenbankname", JOptionPane.QUESTION_MESSAGE);
                if (Datenbanken.checkifDBExists(datenbankName) == true) {
                    JOptionPane.showMessageDialog((JFrame) SwingUtilities.getWindowAncestor((JButton) e.getSource()),
                            "Es gibt bereits eine Datenbank mit diesem Usernamen",
                            "Fehler",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }
                Dotenv dotenv = Dotenv.load();
                String dbAdmin = dotenv.get("dbusername");

                // Create Database
                SQLManager.SQLExceuteUnsafe("CREATE DATABASE " + datenbankName);
                // Gibt dem DBAdmin Create Rechte
                ArrayList parameters = new ArrayList();
                parameters.add(dbAdmin);
                SQLManager.SQLExecute("GRANT CREATE  ON " + datenbankName + ".* TO ?@'%'", parameters);

                JOptionPane.showMessageDialog((JFrame) SwingUtilities.getWindowAncestor((JButton) e.getSource()),
                        "Die Datenbank wurde erstellt",
                        "Erfolg",
                        JOptionPane.INFORMATION_MESSAGE);
                userList.setListData(Datenbanken.getalleDatenbanken().toArray());
            }
        });

        DatenbankenDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JButton btnAdd = (JButton) e.getSource();
                JPanel userPanel = (JPanel) btnAdd.getParent().getParent();
                JList userList = (JList) userPanel.getComponent(0);
                String selectedItem = (String) userList.getSelectedValue();
                if (selectedItem == null) {
                    return;
                }

                Integer dbLöschen = JOptionPane.showConfirmDialog(
                        (JFrame) SwingUtilities.getWindowAncestor((JButton) e.getSource()),
                        "Soll die Datenbank " + selectedItem + " wirklich entfernt werden?",
                        "Rückfrage",
                        JOptionPane.YES_NO_OPTION);

                if (dbLöschen != JOptionPane.YES_OPTION) {
                    return;
                }

                Dotenv dotenv = Dotenv.load();

                // Lösche die Datenbank
                SQLManager.SQLExceuteUnsafe("DROP DATABASE " + selectedItem);

                JOptionPane.showMessageDialog((JFrame) SwingUtilities.getWindowAncestor((JButton) e.getSource()),
                        "Die Datenbank wurde gelöscht",
                        "Erfolg",
                        JOptionPane.INFORMATION_MESSAGE);
                userList.setListData(Datenbanken.getalleDatenbanken().toArray());
            }
        });

        return DatenbankenPanel;
    }
}
