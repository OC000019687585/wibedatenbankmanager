package de.bremen.swing;

import java.awt.Component;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import de.bremen.Anwender;
import de.bremen.WiBePropertiesGenerator;
import io.github.cdimascio.dotenv.Dotenv;

public class WibePropertiesPanel extends JPanel {
    public static JPanel getWibePropertiesPanel() {
        JPanel userPanel = new JPanel();
        userPanel.setLayout(new GridLayout(1, 2));

        JList userListe = new JList<>(Anwender.getallAnwender().toArray());
        JScrollPane userListescrollPane = new JScrollPane();
        userListescrollPane.setViewportView(userListe);
        userListe.setLayoutOrientation(JList.VERTICAL);

        JPanel buttonPanel = new JPanel();

        JButton addButton = new JButton("Erstelle wibe.properties für ausgewählten User", null);
        buttonPanel.add(addButton);

        addButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JButton component = (JButton) e.getSource();
                JFrame frame = (JFrame) SwingUtilities.getRoot(component);
                JPanel userPanel = (JPanel) component.getParent().getParent();
                JList userList = (JList) userPanel.getComponent(0);
                String selectedItem = (String) userList.getSelectedValue();
                if (selectedItem == null) {
                    return;
                }
                Dotenv dotenv = Dotenv.load();
                // Erstelle die WiBe Properties
                WiBePropertiesGenerator wiBePropertiesGenerator = new WiBePropertiesGenerator(selectedItem);
                // Frage wo die Datei gespeichert werden soll
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                if(selectedItem.equals(dotenv.get("dbusername"))){
                    fileChooser.setCurrentDirectory(new File(System.getenv("APPDATA") + "/wibekalkulator"));
                }else{
                    fileChooser.setCurrentDirectory(new File(System.getProperty("user.home") + File.separator + "Desktop"));
                }
                fileChooser.setDialogTitle("Wo soll die wibe.properties gespeichert werden?");
                int option = fileChooser.showSaveDialog(frame);
                if (option == JFileChooser.APPROVE_OPTION) {
                    File ordner = fileChooser.getSelectedFile();
                    writeWiBePropertiesinDatei(ordner, wiBePropertiesGenerator);
                    JOptionPane.showMessageDialog(
                        frame,
                        "Die wibe.properties wurde erfolgreich erstellt.",
                        "Erfolg",
                        JOptionPane.INFORMATION_MESSAGE);
                } else {
                }
            }
        });

        userPanel.add(userListe);
        userPanel.add(buttonPanel);
        return userPanel;
    }

    private static Component findComponentbyName(Component component, String componentName) {

        JFrame frame = (JFrame) SwingUtilities.getRoot(component);

        for (Component item : getAllComponents(frame)) {
            if (item.getName() == componentName) {
                return item;
            }
        }

        return new Component() {

        };
    }

    public static List<Component> getAllComponents(final Container c) {
        Component[] comps = c.getComponents();
        List<Component> compList = new ArrayList<Component>();
        for (Component comp : comps) {
            compList.add(comp);
            if (comp instanceof Container)
                compList.addAll(getAllComponents((Container) comp));
        }
        return compList;
    }

    private static void writeWiBePropertiesinDatei(File Ordner, WiBePropertiesGenerator wiBePropertiesGenerator) {
        try (PrintWriter pWriter = new PrintWriter(new FileWriter(Ordner + File.separator + "wibe.properties"));) {
            pWriter.println(wiBePropertiesGenerator.getWibeProperties());
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
