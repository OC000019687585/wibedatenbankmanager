package de.bremen.swing;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import de.bremen.Anwender;
import io.github.cdimascio.dotenv.Dotenv;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class UserPanel extends JPanel {

    public static JPanel getUserPanel() {
        JPanel userPanel = new JPanel();
        userPanel.setLayout(new GridLayout(1, 2));

        JList userListe = new JList<>(Anwender.getallAnwender().toArray());
        JScrollPane userListescrollPane = new JScrollPane();
        userListescrollPane.setViewportView(userListe);
        userListe.setLayoutOrientation(JList.VERTICAL);
        JPanel buttonPanel = new JPanel();

        JButton userAdd = new JButton("Datenbankbenutzer hinzufügen");
        JButton userDelete = new JButton("Datenbankbenutzer löschen");
        userDelete.setToolTipText("Löscht den Datenbankbenutzer und entfernt alle berechtigungen");

        userPanel.add(userListe);
        buttonPanel.add(userAdd);
        buttonPanel.add(userDelete);
        userPanel.add(buttonPanel);

        userDelete.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JButton btnuserDelete = (JButton) e.getSource();
                JPanel userPanel = (JPanel) btnuserDelete.getParent().getParent();
                JList userList = (JList) userPanel.getComponent(0);
                String selectedItem = (String) userList.getSelectedValue();
                if (selectedItem == null) {
                    return;
                }

                Integer userLöschen = JOptionPane.showConfirmDialog(
                        (JFrame) SwingUtilities.getWindowAncestor((JButton) e.getSource()),
                        "Soll der User " + selectedItem + " wirklich entfernt werden?",
                        "Rückfrage",
                        JOptionPane.YES_NO_OPTION);

                if (userLöschen != JOptionPane.YES_OPTION) {
                    return;
                }

                Dotenv dotenv = Dotenv.load();
                if (selectedItem.equals(dotenv.get("dbusername"))) {
                    JOptionPane.showMessageDialog((JFrame) SwingUtilities.getWindowAncestor((JButton) e.getSource()),
                            "Der Datenbankadmin kann nicht gelöscht werden.",
                            "Fehler",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }
                try {
                    Anwender.deleteAnwender(selectedItem);
                    userList.setListData(Anwender.getallAnwender().toArray());
                } catch (Exception err) {
                    JOptionPane.showMessageDialog((JFrame) SwingUtilities.getWindowAncestor((JButton) e.getSource()),
                            err.getLocalizedMessage(),
                            "Fehler",
                            JOptionPane.ERROR_MESSAGE);
                    err.printStackTrace();
                }
            }
        });

        userAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // ToDo: Prüfe Anwendername auf validität
                String anwenderUsername = JOptionPane.showInputDialog(
                        (JFrame) SwingUtilities.getWindowAncestor((JButton) e.getSource()),
                        "Benutzername der hinzugefügt werden soll.",
                        "Username", JOptionPane.QUESTION_MESSAGE);
                // Prüfe ob es den User schon gibt
                if (Anwender.checkIfUserExists(anwenderUsername) == true) {
                    JOptionPane.showMessageDialog((JFrame) SwingUtilities.getWindowAncestor((JButton) e.getSource()),
                            "Es gibt bereits einen Benutzer mit diesem Usernamen",
                            "Fehler",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }
                try {
                    Anwender.addAnwender(anwenderUsername);
                    JButton btnuserAdd = (JButton) e.getSource();
                    JPanel userPanel = (JPanel) btnuserAdd.getParent().getParent();
                    JList userList = (JList) userPanel.getComponent(0);
                    userList.setListData(Anwender.getallAnwender().toArray());
                    JOptionPane.showMessageDialog((JFrame) SwingUtilities.getWindowAncestor((JButton) e.getSource()),
                            "Der Benutzer wurde angelegt.",
                            "Erfolg",
                            JOptionPane.INFORMATION_MESSAGE);
                } catch (SQLException err) {
                    JOptionPane.showMessageDialog((JFrame) SwingUtilities.getWindowAncestor((JButton) e.getSource()),
                            err.getLocalizedMessage(),
                            "Fehler",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
        });

        return userPanel;
    }

}
