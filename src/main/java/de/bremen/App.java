package de.bremen;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import de.bremen.swing.BerechtigungenPanel;
import de.bremen.swing.DatenbankPanel;
import de.bremen.swing.UserPanel;
import de.bremen.swing.WibePropertiesPanel;

import java.awt.GridLayout;
import java.util.Properties;

public final class App {
    private App() {
    }

/**
 * @param aArgs
 * @see http://www.javapractices.com/topic/TopicAction.do?Id=231
 */
public static void main(String... aArgs){
    App app = new App();
    app.buildAndDisplayGui();
  }
  
  private void buildAndDisplayGui(){
    final Properties properties = new Properties();
    String frameTitle;
    try {
      properties.load(this.getClass(). getClassLoader().getResourceAsStream("project.properties"));
      frameTitle = properties.getProperty("project.name") + " - " + properties.getProperty("project.version");
    } catch (Exception e) {
      frameTitle = "";
    }

    JFrame frame = new JFrame(frameTitle); 
    frame.setLayout(new GridLayout(1,1));
    buildContent(frame);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setBounds(50, 50, 800, 600);
    frame.setVisible(true);
  }
  
  private void buildContent(JFrame aFrame){
    JTabbedPane tabbedPane = new JTabbedPane();   
    JPanel panelUsers = UserPanel.getUserPanel();
    JPanel panelDBs = DatenbankPanel.getDatenbankenPanel();
    JPanel panelWiBeProb = WibePropertiesPanel.getWibePropertiesPanel();
    JPanel panelBerechtigungen = BerechtigungenPanel.getBerechtigungenPanel();
    JPanel panelConfig = new JPanel();
    tabbedPane.addTab("Benutzer", panelUsers);
    tabbedPane.addTab("Datenbanken", panelDBs);
    tabbedPane.addTab("Berechtigungen", panelBerechtigungen);
    tabbedPane.addTab("WiBe.properties", panelWiBeProb);
    tabbedPane.addTab("Config", panelConfig);
    
    aFrame.add(tabbedPane);
  }
  
}
