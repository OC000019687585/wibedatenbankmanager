package de.bremen;

import java.security.SecureRandom;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

// https://www.baeldung.com/java-generate-secure-password
public class Passwortgenerator {

    public static String generateSecureRandomPassword() {
        Stream<Character> pwdStream = Stream.concat(getRandomNumbers(2),
                Stream.concat(getRandomSpecialChars(2),
                        Stream.concat(getRandomAlphabets(2, true), getRandomAlphabets(4, false))));
        List<Character> charList = pwdStream.collect(Collectors.toList());
        Collections.shuffle(charList);
        String password = charList.stream()
                .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
                .toString();
        return password;
    }

    private static Stream<Character> getRandomSpecialChars(int count) {
        Random random = new SecureRandom();
        IntStream specialChars = random.ints(count, 33, 45);
        specialChars = specialChars.filter(item -> (item != 34)); //entfernt das "
        specialChars = specialChars.filter(item -> (item != 39)); //entfernt das '
        return specialChars.mapToObj(data -> (char) data);
    }

    private static Stream<Character> getRandomNumbers(int count) {
        Random random = new SecureRandom();
        IntStream specialChars = random.ints(count, 48, 57);
        return specialChars.mapToObj(data -> (char) data);
    }

    private static Stream<Character> getRandomAlphabets(int count, boolean allCaps) {
        Random random = new SecureRandom();
        IntStream specialChars;
        if (allCaps == true) {
            specialChars = random.ints(count, 97, 122);
        } else {
            specialChars = random.ints(count, 65, 90);
        }
        return specialChars.mapToObj(data -> (char) data);
    }

}
