package de.bremen;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

import io.github.cdimascio.dotenv.Dotenv;

public class Anwender {
    String anmeldename;
    LocalDate erstelltAm;
    LocalDate bearbeitetAm;
    LocalDate gelöschtAm;

    public Anwender(String anmeldename) {
        this.anmeldename = anmeldename;
    }

    public static ArrayList<String> getallAnwender() {
        ArrayList<String> anwenders = new ArrayList<String>();

        // Ermitle den SQL Query für die genutze Datenbank
        Dotenv dotenv = Dotenv.load();
        String sqlString = "";

        // ToDo: andere Datenbanken einfügen
        switch (dotenv.get("dbdialekt")) {
            case "mariadb":
                sqlString = "Select user, host from mysql.user where host = '%'";
                break;
            default:
                break;
        }
        try {
            ResultSet rs = SQLManager.SQLSelect(sqlString, new ArrayList());
            while (rs.next()) {
                anwenders.add(rs.getString(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return anwenders;
    }

    public static void deleteAnwender(String anwenderUsername) throws SQLException {
        // Ermitle den SQL Query für die genutze Datenbank
        Dotenv dotenv = Dotenv.load();
        String sqlString = "";

        if (anwenderUsername.equals(dotenv.get("dbusername"))) {
            return;
        }

        // ToDo: andere Datenbanken einfügen
        switch (dotenv.get("dbdialekt")) {
            case "mariadb":
                sqlString = "drop user ?@'%'";
                break;
            default:
                break;
        }

        ArrayList parameter = new ArrayList<>();
        parameter.add(anwenderUsername);

        // entfernt den user aus der db
        SQLManager.SQLExecute(sqlString, parameter);
        // entfernt den user aus der wibedbmanager.datenbankuser
        SQLManager.SQLExecute("delete from wibedbmanager.datenbankuser where username = ?", parameter);
    }

    public static boolean checkIfUserExists(String anwenderUsername) {
        ArrayList parameter = new ArrayList();
        parameter.add(anwenderUsername);
        try {
            ResultSet rs = SQLManager.SQLSelect("Select username from wibedbmanager.datenbankuser where username = ?",
                    parameter);
            while (rs.next()) {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    public static void addAnwender(String anwenderUsername) throws SQLException {
        // passwort genierieren
        String anwenderPW = Passwortgenerator.generateSecureRandomPassword();
        ArrayList parameters = new ArrayList();
        parameters.add(anwenderUsername);
        parameters.add(anwenderPW);
        // passwort speichern
        SQLManager.SQLExecute("Insert into wibedbmanager.datenbankuser (username, passwort) values (?,?)", parameters);
        // user anlegen
        // Ermitle den SQL Query für die genutze Datenbank
        Dotenv dotenv = Dotenv.load();
        String sqlString = "";
        // ToDo: andere Datenbanken einfügen
        switch (dotenv.get("dbdialekt")) {
            case "mariadb":
                sqlString = "create user ?@'%' identified by ?;";
                break;
            default:
                break;
        }
        SQLManager.SQLExecute(sqlString, parameters);
        // user auf testdb berechtigen
        String testdbname = dotenv.get("testdbname");
        parameters = new ArrayList();
        parameters.add(testdbname);
        parameters.add(anwenderUsername);
        // ToDo: andere Datenbanken einfügen
        switch (dotenv.get("dbdialekt")) {
            case "mariadb":
                sqlString = "GRANT select, insert, update, delete on TESTDB.* to ?@'%'";
                sqlString = sqlString.replace("TESTDB", testdbname);
                parameters = new ArrayList();
                parameters.add(anwenderUsername);
                break;
            default:
                break;
        }
        SQLManager.SQLExecute(sqlString, parameters);
    }

    public static String getPWforUser(String username) {
        ArrayList parameters = new ArrayList();
        parameters.add(username);
        
        String pw = "";
        try {
            ResultSet rs = SQLManager.SQLSelect("SELECT passwort FROM wibedbmanager.datenbankuser WHERE username = ?", parameters);
            rs.first();
            pw = rs.getString(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pw;
    }

}
